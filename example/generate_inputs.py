import numpy as np
import pandas as pd
from agrosim.io import write_json
from agrosim.json_schema import schema
from spa.formalism.soil import eval_alpha
from spa.params import Params

# weather
weather_raw = pd.read_csv('PechRouge2017_11170004.csv', sep=';', header=0)
weather_raw.replace([-999, ], [np.nan, ], inplace=True)
weather_raw['date'] = pd.to_datetime(weather_raw['date'], format="%d/%m/%Y %H:%M")

cols_name = {'date': 'date',
             'rainfall': 'rain',
             'relative_humidity': 'rh',
             'temperature': 'temp',
             'wind_speed': 'ws'}
weather = weather_raw.rename(columns=cols_name)[cols_name.values()]
weather['rg'] = (weather_raw['radiation_direct'] + weather_raw['radiation_diffuse']) / 100  # [J cm-2] to [MJ m-2]

# format overall inputs
inputs = dict(
    field=dict(
        area=1e4,
        density=1 / 0.8 ** 2,
        crop="courgette",
        latitude=43.14428,
        longitude=3.135533,
        variety="Nice"
    ),
    soil=dict(
        voxels=[
            dict(
                thickness=20,
                theta_fc=0.3,
                theta_pwp=0.2,
                theta_res=0.2 * 0.99,
                theta_sat=0.5,
                n=2,
            ),
            dict(
                thickness=600,
                theta_fc=0.33,
                theta_pwp=0.17,
                theta_res=0.17 * 0.99,
                theta_sat=0.46,
                n=2,
            )
        ]
    ),
    weather=[row.to_dict() for _, row in weather.iterrows()],
    state=dict(
        thetas=[0, 0],
        wfs=[0, 0]
    )
)

# compute vg params for each layer
psi_pwp = Params().plant.psi_pwp  # [MPa]
for i, layer in enumerate(inputs['soil']['voxels']):
    layer['volume'] = inputs['field']['area'] * layer['thickness'] * 1e-3
    layer['alpha'] = eval_alpha(psi_pwp, layer['theta_pwp'], layer['theta_res'], layer['theta_sat'], 1 / layer['n'])
    # initialize water content to field capacity
    inputs['state']['thetas'][i] = layer['theta_fc']

for entry in inputs["weather"]:
    entry["date"] = entry["date"].isoformat()

sch = schema(Params(), require='inputs')
write_json(inputs, "inputs_example.json", schema=sch)
