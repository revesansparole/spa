import pandas as pd
from agrosim.io import read_json
from agrosim.process import MemoryProcess, one_hour
from spa.params_management import instantiate_params
from spa.simu.model import Model
from spa.simu.simulator import Simulator


class ProcessOutput(MemoryProcess):
    def update(self, t, model, params):
        del params

        entry = dict(
            date=t,
            drainage=model.soil.drainage,
            evap=model.soil.evap,
            irrig=sum(model.soil.irrig),
            psi_atm=model.atm.potential,
            psi_xylem=model.plant.potential,
            rain=model.atm.rain,
            rg=model.atm.rg,
            rh=model.atm.rh,
            runoff=model.soil.runoff,
            transpi=model.plant.transpi,
            transpi_pot=model.plant.transpi_pot,
            uptake=model.soil.uptake,
        )

        for i, theta in enumerate(model.soil.thetas):
            entry[f'theta_{i:02d}'] = theta
        for i, psi in enumerate(model.soil.potentials):
            entry[f'psi_soil_{i:02d}'] = psi

        self.append(entry)

        return t + one_hour


params = instantiate_params(read_json("inputs_example.json"))


def planif(t, forecast):
    doy = t.timetuple().tm_yday

    if doy < params.plant.plantation or doy > params.plant.senescence:
        return [0.] * 7

    if doy < params.plant.maturity:
        transpi = 5 / 7  # [L.day-1]
    else:
        transpi = 15 / 7  # [L.day-1]

    plant_needs = [transpi * 1e-3 * params.field.density] * 7
    evap = 1500 / 138  # [m3.field-1]

    irrigs = [v * params.field.area + evap for v in plant_needs]

    return irrigs


params.planif = planif

# perform computation
simu = Simulator(Model(params), params)
pout = ProcessOutput()
simu.add_process(pout)

simu.run()

hdf = pd.DataFrame(pout.retrieve()).set_index('date')
