"""
Xylem potential
===============

Plot xylem water potential according to various explanatory variables
"""
from datetime import datetime, timedelta

import matplotlib.pyplot as plt

from simu_run import hdf, params

# restrict plot to plant growing period
doy0 = datetime(params.weather[0].date.year, 1, 1)
t_ini = doy0 + timedelta(days=params.plant.plantation - 1)
t_fin = doy0 + timedelta(days=params.plant.senescence - 1)

hdf = hdf.loc[t_ini:t_fin]

hdf_night = hdf.loc[(hdf.index.hour < 4) | (hdf.index.hour > 20)]
hdf_midday = hdf.loc[(hdf.index.hour < 14) & (hdf.index.hour > 10)]

# plot result
fig, axes = plt.subplots(1, 2, sharey='all', figsize=(12, 8), squeeze=False)

ax = axes[0, 0]
ax.plot(hdf_night['psi_atm'], hdf_night['psi_xylem'], 'o', label="night")
ax.plot(hdf_midday['psi_atm'], hdf_midday['psi_xylem'], 'o', label="midday")
ax.legend(loc='upper left')
ax.set_xlabel("atm water potential [MPa]")
ax.set_ylabel("xylem water potential [MPa]")

ax = axes[0, 1]
ax.plot(hdf_night[f'psi_soil_{params.soil.root_voxel:02d}'], hdf_night['psi_xylem'], 'o', label="night")
ax.plot(hdf_midday[f'psi_soil_{params.soil.root_voxel:02d}'], hdf_midday['psi_xylem'], 'o', label="midday")
ax.legend(loc='upper left')
ax.set_xlabel("soil water potential [MPa]")

fig.tight_layout()
plt.show()
