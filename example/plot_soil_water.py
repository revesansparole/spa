"""
Soil water
==========

Plot soil water related components
"""
import matplotlib.pyplot as plt

from simu_run import hdf, params

fig, axes = plt.subplots(4, 1, sharex='all', figsize=(12, 8), squeeze=False)

ax = axes[0, 0]
rain = hdf.loc[hdf['rain'] > 0, 'rain']
ax.plot(rain.index, rain, 'o')
ax.set_ylabel("[mm]")

ax = ax.twinx()
ax.plot(hdf.index, hdf['rain'].cumsum())

ax = axes[1, 0]
irrig = hdf.loc[hdf['irrig'] > 0, 'irrig']
ax.plot(irrig.index, irrig, 'o')
ax.set_ylabel("[m3]")

ax = axes[2, 0]
for i, vox in enumerate(params.soil.voxels):
    ax.plot(hdf.index, hdf[f'theta_{i:02d}'], label=f"vox{i:02d}")

ax.legend(loc='upper left')
ax.set_ylabel("[m3.m-3]")

ax = axes[3, 0]
ax.plot(hdf.index, hdf['evap'], label="evaporation")
ax.plot(hdf.index, hdf['runoff'], label="runoff")
ax.plot(hdf.index, hdf['drainage'], label="drainage")

ax.legend(loc='upper left')
ax.set_ylabel("[m3]")

fig.tight_layout()
plt.show()
