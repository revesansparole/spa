"""
Gradient of potentials
======================

Plot evolution of gradient of potential (soil->plant->atmosphere) over time
"""
from datetime import datetime, timedelta

import matplotlib.pyplot as plt

from simu_run import hdf, params

# restrict plot to plant growing period
doy0 = datetime(params.weather[0].date.year, 1, 1)
t_ini = doy0 + timedelta(days=params.plant.plantation - 1)
t_fin = doy0 + timedelta(days=params.plant.senescence - 1)

hdf = hdf.loc[t_ini:t_fin]

# plot result
fig, axes = plt.subplots(4, 1, sharex='all', figsize=(12, 8), squeeze=False)

ax = axes[0, 0]
ax.plot(hdf.index, hdf['psi_atm'])

ax.set_ylabel("[MPa]")

ax = axes[1, 0]
ax.plot(hdf.index, hdf['psi_xylem'])

ax.set_ylabel("[MPa]")

ax = axes[2, 0]
ax.plot(hdf.index, hdf[f'psi_soil_{params.soil.root_voxel:02d}'], label=f"root voxel")
ax.legend(loc='upper left')
ax.set_ylabel("[MPa]")

ax = axes[3, 0]
for i, vox in enumerate(params.soil.voxels):
    ax.plot(hdf.index, hdf[f'psi_soil_{i:02d}'], label=f"vox{i:02d}")

ax.set_ylim(-1.5, 0.)
ax.legend(loc='upper left')
ax.set_ylabel("[MPa]")

fig.tight_layout()
plt.show()
