"""
Transpiration
=============

Plot plant components related to transpiration
"""
from datetime import datetime, timedelta

import matplotlib.pyplot as plt

from simu_run import hdf, params

# restrict plot to plant growing period
doy0 = datetime(params.weather[0].date.year, 1, 1)
plantation = doy0 + timedelta(days=params.plant.plantation - 1)
maturity = doy0 + timedelta(days=params.plant.maturity - 1)
senescence = doy0 + timedelta(days=params.plant.senescence - 1)

hdf = hdf.loc[plantation:senescence]

# statistics
nb_weeks_vege = (maturity - plantation).days / 7
upt_vege = hdf.loc[plantation:maturity, 'uptake'].sum()
upt_vege *= 1e3 / params.field.area / params.field.density / nb_weeks_vege  # [L.plant-1.week-1]

nb_weeks_maturity = (senescence - maturity).days / 7
upt_maturity = hdf.loc[maturity:senescence, 'uptake'].sum()
upt_maturity *= 1e3 / params.field.area / params.field.density / nb_weeks_maturity  # [L.plant-1.week-1]

print(f"uptake vege: {upt_vege:.1f} [L.plant-1.week-1]")
print(f"uptake matu: {upt_maturity:.1f} [L.plant-1.week-1]")

etc_vege = hdf.loc[plantation:maturity, 'uptake'].sum() + hdf.loc[plantation:maturity, 'evap'].sum()
etc_vege *= 1e3 / params.field.area / nb_weeks_vege
etc_maturity = hdf.loc[maturity:senescence, 'uptake'].sum() + hdf.loc[maturity:senescence, 'evap'].sum()
etc_maturity *= 1e3 / params.field.area / nb_weeks_maturity

print(f"ETc vege: {etc_vege:.1f} [mm]")
print(f"ETc maturity: {etc_maturity:.1f} [mm]")

# plot result
fig, axes = plt.subplots(3, 1, sharex='all', figsize=(12, 9), squeeze=False, gridspec_kw={'height_ratios': [3, 1, 1]})

ax = axes[0, 0]
ax.plot(hdf.index, hdf['transpi'] * 1e3, label="transpi")
ax.plot(hdf.index, hdf['transpi_pot'] * 1e3, label="transpi_pot")

ax.legend(loc='upper left')
ax.set_ylabel("[L.plant-1.h-1]")

ax = axes[1, 0]
uptake_field = hdf['uptake'].resample('1D').sum()
ax.plot(uptake_field.index, uptake_field)
ax.set_ylabel("[m3.day-1]")

ax = axes[2, 0]
uptake_plant = uptake_field / params.field.area / params.field.density * 1e3
ax.plot(uptake_plant.index, uptake_plant)
ax.set_ylabel("[L.day-1]")

fig.tight_layout()
plt.show()
