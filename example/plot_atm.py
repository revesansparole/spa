"""
Atmosphere
==========

Plot weather data
"""
import matplotlib.pyplot as plt

from simu_run import hdf

fig, axes = plt.subplots(3, 1, sharex='all', figsize=(12, 5), squeeze=False)

ax = axes[0, 0]
ax.plot(hdf.index, hdf['rg'])
ax.set_ylabel("[W.m-2]")

ax = axes[1, 0]
ax.plot(hdf.index, hdf['rh'])
ax.set_ylabel("[%]")

ax = axes[2, 0]
ax.plot(hdf.index, hdf['psi_atm'])
ax.set_ylabel("[MPa]")

fig.tight_layout()
plt.show()
