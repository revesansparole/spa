"""
Stomatal conductance
====================

plot scaling of stomatal conductance with xylem potential
"""
import numpy as np
import matplotlib.pyplot as plt
from spa.formalism.plant import wp_stress

psi_ref = -1.9
sensitivity = 3.2

psis = np.linspace(-1.5, 0., 100)
scas = [wp_stress(psi, psi_ref, sensitivity) for psi in psis]

# plot result
fig, axes = plt.subplots(1, 1, figsize=(10, 6), squeeze=False)

ax = axes[0, 0]
ax.plot(psis, scas, label="Tuzet")
ax.plot(psis, [wp_stress(psi, -0.9, sensitivity) for psi in psis], label=f"psi_ref: {-0.9:.1f}")
ax.plot(psis, [wp_stress(psi, -0.5, sensitivity) for psi in psis], label=f"psi_ref: {-0.5:.1f}")
ax.plot(psis, [wp_stress(psi, -0.2, 5) for psi in psis], label=f"psi_ref: {-0.2:.1f}")
ax.legend()
ax.set_xlabel("[MPa]")
ax.set_ylabel("[-]")

fig.tight_layout()
plt.show()
