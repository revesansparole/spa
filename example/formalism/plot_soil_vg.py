"""
Van Genuchten formalism
=======================

plot soil moisture relation to matric potential
"""
import numpy as np
import matplotlib.pyplot as plt
from spa.formalism.soil import vg_psi, vg_theta, eval_alpha

# compute thetas and psis from VG formalism
theta_res = 0.1  # [m3.m-3]
theta_pwp = theta_res / 0.9  # [m3.m-3]
theta_fc = 0.37  # [m3.m-3]
theta_sat = 0.5  # [m3.m-3]

vg_n = 2
vg_m = 0.5
vg_alpha = 0.005 * 1e4  # [cm-1] to [MPa-1]

psis_exp = np.linspace(-4., 2., 100)
thetas = [vg_theta(-10 ** psi, theta_res, theta_sat, vg_alpha, vg_n, vg_m) for psi in psis_exp]

# use pwp to estimate alpha
vg_alpha = eval_alpha(-1.5, theta_pwp, theta_res, theta_sat, 1 / vg_n)
thetas_estim = [vg_theta(-10 ** psi, theta_res, theta_sat, vg_alpha, vg_n) for psi in psis_exp]

thetas_op = np.linspace(theta_pwp, theta_sat * 0.999, 100)
psis_op = [vg_psi(theta, theta_res, theta_sat, 1 / vg_alpha, 1 / vg_n) for theta in thetas_op]

# plot result
fig, axes = plt.subplots(1, 2, figsize=(12, 6), squeeze=False)

ax = axes[0, 0]
ax.plot(thetas, [v + 4 for v in psis_exp], label="ori VG")
ax.plot(thetas_estim, [v + 4 for v in psis_exp], label="pwp1.5")
ax.plot(thetas_op, [np.log10(-v) + 4 for v in psis_op], label="op")
ax.legend()
ax.set_xlabel("[m3.m-3]")
ax.set_ylabel("[log10(hPa)] aka [log10(cm)]")

ax = axes[0, 1]
ax.plot(thetas, [-(10 ** v) for v in psis_exp], label="ori VG")
ax.plot(thetas_op, psis_op, label="op")
ax.legend()
ax.set_xlabel("[m3.m-3]")
ax.set_ylabel("[MPa]")
ax.set_ylim(-1.5, 0)

fig.tight_layout()
plt.show()
