"""
Van Genuchten formalism for Kr
==============================

plot soil moisture relation to Kr
"""
import numpy as np
import matplotlib.pyplot as plt
from spa.formalism.soil import vg_kr, vg_theta

# compute thetas and psis from VG formalism
theta_res = 0.1  # [m3.m-3]
theta_pwp = theta_res / 0.9  # [m3.m-3]
theta_fc = 0.37  # [m3.m-3]
theta_sat = 0.5  # [m3.m-3]

vg_n = 2
vg_alpha = 0.005 * 1e4  # [cm-1] to [MPa-1]

psis_exp = np.linspace(-4., 2., 100)
thetas = [vg_theta(-10 ** psi, theta_res, theta_sat, vg_alpha, vg_n) for psi in psis_exp]

krs = [vg_kr(th, theta_res, theta_sat, vg_n) for th in thetas]

# plot result
fig, axes = plt.subplots(1, 2, sharey='all', figsize=(12, 6), squeeze=False)

ax = axes[0, 0]
ax.plot([v + 4 for v in psis_exp], krs, label="ori VG")
ax.set_xlabel("[log10(hPa)] aka [log10(cm)]")

ax = axes[0, 1]
ax.plot(thetas, krs, label="ori VG")
ax.set_xlabel("[m3.m-3]")

fig.tight_layout()
plt.show()
