"""
Indicators to plan irrigation
=============================

Plot set of indicators to help with planification
"""
from datetime import datetime, timedelta
import matplotlib.pyplot as plt

from simu_run import hdf, params

# restrict plot to plant growing period
doy0 = datetime(params.weather[0].date.year, 1, 1)
t_ini = doy0 + timedelta(days=params.plant.plantation - 1)
t_fin = doy0 + timedelta(days=params.plant.senescence - 1)

hdf = hdf.loc[t_ini:t_fin]

# plot result
fig, axes = plt.subplots(2, 1, sharex='all', figsize=(12, 8), squeeze=False)

ax = axes[0, 0]
ax.plot(hdf.index, hdf['psi_xylem'])
ax.set_ylabel("[MPa]")

ax = axes[1, 0]
ax.plot(hdf.index, hdf['evap'].cumsum(), label="evaporation")
ax.plot(hdf.index, hdf['runoff'].cumsum(), label="runoff")
ax.plot(hdf.index, hdf['drainage'].cumsum(), label="drainage")
ax.plot(hdf.index, hdf['uptake'].cumsum(), '--', label="uptake")
ax.plot(hdf.index, hdf['irrig'].cumsum(), '--', label="irrig")

ax.legend(loc='upper left')
ax.set_ylabel("[m3]")

fig.tight_layout()
plt.show()
