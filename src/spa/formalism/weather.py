"""
Formalisms that should be moved in weather but put here due to lack of time
"""
from math import log

from weather.constants import gas_constant

water_molar_volume = 18e-6
"""[m3.mol-1] Molar volume of water around 20°C

References: 18 [g.mol-1] and 1000 [g.L-1] @ 20°C
"""


def water_potential(temperature, relative_humidity):
    """Water potential in the atmosphere

    Raises: ValueError if relative_humidity is exactly 0

    Args:
        temperature (float): [°C] air temperature
        relative_humidity(float): [%] Relative humidity of the air (between 0 and 100)

    Returns:
        (float): [MPa]
    """
    return gas_constant * (temperature + 273.15) / water_molar_volume * log(relative_humidity / 100) * 1e-6
