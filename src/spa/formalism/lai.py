import numpy as np


def lai_traj(x, lai_max, doy_max, spread):
    """Gaussian function used as LAI trajectory

    References:
        Netzer et al. (2009)

     Args:
        x (float): [-] month
        lai_max (float): [-] height of the curve's peak
        doy_max (float): [-] position of the center of the peak as day of year
        spread (float): [-] controls the width of the "bell" (i.e. standard deviation)

    Returns:
        (float): [m2 m-2] Leaf Area Index
    """

    return lai_max * np.exp(-(x - doy_max) ** 2 / (2 * spread ** 2))
