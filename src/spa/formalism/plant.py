"""
Formalism related to plant physiology
"""
from math import exp


def wp_stress(psi, psi_ref, sensitivity):
    """Sensitivity of stomata to xylem water potential

    References:
          Tuzet et al. (2003)

    Args:
        psi (float): [MPa] Xylem water potential
        psi_ref (float): [MPa] reference potential below which stomata close
        sensitivity (float): [MPa-1] slope of relation

    Returns:
        (float): [-] scaling between 0 and 1
    """
    num = (1 + exp(sensitivity * psi_ref))
    denom = (1 + exp(sensitivity * (psi_ref - psi)))
    return num / denom


def light_stress(rg, light_sat):
    """Sensitivity of stomata to light

    Args:
        rg (float): [W.m-2] light intensity
        light_sat (float): [W.m-2] light intensity above which stomata opening saturate

    Returns:
        (float): [-] scaling between 0 and 1
    """
    return min(rg / light_sat, 1.)
