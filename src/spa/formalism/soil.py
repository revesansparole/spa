"""
Formalisms that should have been moved in soil but weren't for lack of time
"""


def vg_kr(theta, theta_res, theta_sat, n):
    """Van Genuchten formalism to compute water related conductivity coefficient

    References:
        - Van Genuchten (1980)

    Args:
        theta (float): [m3.m-3] soil moisture
        theta_res (float): [m3.m-3] residual soil moisture
        theta_sat (float): [m3.m-3] soil moisture at saturation
        n (float): [-] shape parameter

    Returns:
        (float): [-] scaling factor for Ks
    """
    m = 1 - 1 / n

    sat = (theta - theta_res) / (theta_sat - theta_res)
    sat = min(max(1e-3, sat), 1.)

    return sat ** 0.5 * (1 - (1 - sat ** (1 / m)) ** m) ** 2


def vg_theta(psi, theta_res, theta_sat, alpha, n, m=None):
    """Van Genuchten formalism to compute soil moisture

    References:
        - Van Genuchten (1980)

    Args:
        psi (float): [MPa] matric potential
        theta_res (float): [m3.m-3] residual soil moisture
        theta_sat (float): [m3.m-3] soil moisture at saturation
        alpha (float): [MPa-1] scaling parameter
        n (float): [-] shape parameter
        m (float): [-] shape parameter

    Returns:
        (float): [m3.m-3] soil moisture
    """
    if m is None:
        m = 1 - 1 / n

    sat = (1 / (1 + (alpha * -psi) ** n)) ** m

    return theta_res + sat * (theta_sat - theta_res)


def vg_psi(theta, theta_res, theta_sat, alpha_inv, n_inv, m_inv=None):
    """Van Genuchten formalism to compute matric potential

    References:
        - Van Genuchten (1980)

    Args:
        theta (float): [m3.m-3] soil moisture
        theta_res (float): [m3.m-3] residual soil moisture
        theta_sat (float): [m3.m-3] soil moisture at saturation
        alpha_inv (float): [MPa] scaling parameter
        n_inv (float): [-] shape parameter
        m_inv (float): [-] shape parameter

    Returns:
        (float): [MPa] soil matric potential
    """
    if m_inv is None:
        m_inv = 1 / (1 - n_inv)

    sat = (theta - theta_res) / (theta_sat - theta_res)
    sat = min(max(1e-3, sat), 1.)

    return -alpha_inv * (1 / sat ** m_inv - 1) ** n_inv


def eval_alpha(psi_pwp, theta_pwp, theta_res, theta_sat, n_inv, m_inv=None):
    """Estimation of alpha param in Van Genuchten formalism

    References:
        - Van Genuchten (1980)

    Args:
        psi_pwp (float): [MPa] soil matric potential at plant wilting point (usual -1.5MPa)
        theta_pwp (float): [m3.m-3] soil moisture at plant wilting point
        theta_res (float): [m3.m-3] residual soil moisture
        theta_sat (float): [m3.m-3] soil moisture at saturation
        n_inv (float): [-] shape parameter
        m_inv (float): [-] shape parameter

    Returns:
        (float): [MPa-1] scaling parameter
    """
    if m_inv is None:
        m_inv = 1 / (1 - n_inv)

    sat_pwp = (theta_pwp - theta_res) / (theta_sat - theta_res)

    return (1 / sat_pwp ** m_inv - 1) ** n_inv / -psi_pwp


def compute_drain(soil, thetas):
    """Compute fluxes in soil occurring for a period of time.

    Notes: bottom drainage corresponds to the last computed flux

    Args:
        soil (list[SoilVoxel]): Column of Soil. First element being the top most voxel
        thetas (List[float]): [m3.m-3] Soil moisture in each soil voxel

    Returns:
        (tuple):
        - thetas (List[float]): [m3.m-3] New soil moisture in each soil voxel
        - drains (List[float]): [m3] amount of water leaving each voxel by gravity
    """
    # compute fluxes
    fluxes = [0.] + [max(0., theta - vox.theta_fc) * vox.volume for theta, vox in zip(thetas, soil)]

    # apply fluxes
    thetas_new = [0.] * len(soil)
    for i in range(len(soil) - 1, -1, -1):  # start from bottom voxel
        vox = soil[i]
        theta_new = thetas[i] + (fluxes[i] - fluxes[i + 1]) / vox.volume
        if theta_new > vox.theta_sat:
            fluxes[i] -= (theta_new - vox.theta_sat) * vox.volume
            thetas_new[i] = vox.theta_sat
        else:
            thetas_new[i] = theta_new

    return thetas_new, fluxes[1:]


def compute_water_input(soil, thetas, in_fluxes, thetas_max):
    """Compute the balance in each voxel after adding some water.

    Args:
        soil (list[SoilVoxel]): Column of Soil. First element being the top most voxel
        thetas (List[float]): [m3.m-3] Soil moisture in each soil voxel
        in_fluxes (List[float]): [mm] Amount of water potentially entering each voxel
        thetas_max (List[float]): [mm] Maximum allowed quantity of water in each voxel

    Returns:
        (tuple):
        - thetas (List[float]): [m3.m-3] New soil moisture in each soil voxel
        - in_fluxes (List[float]): [mm] Amount of water really entering each voxel
    """
    thetas_new = []
    fluxes_actual = []
    for theta, in_flux, vox, theta_max in zip(thetas, in_fluxes, soil, thetas_max):
        theta_new = theta + in_flux / vox.volume
        if theta_new > theta_max:
            fluxes_actual.append((theta_max - theta) * vox.volume)
            thetas_new.append(theta_max)
        else:
            fluxes_actual.append(in_flux)
            thetas_new.append(theta_new)

    return thetas_new, fluxes_actual


def compute_water_output(soil, thetas, out_fluxes, thetas_min):
    """Compute the balance in each voxel after removing some water.

    Args:
        soil (list[SoilVoxel]): Column of Soil. First element being the top most voxel
        thetas (List[float]): [m3.m-3] Soil moisture in each soil voxel
        out_fluxes (List[float]): [mm] Amount of water potentially leaving each voxel
        thetas_min (List[float]): [mm] Minimum allowed quantity of water in each voxel

    Returns:
        (tuple):
        - thetas (List[float]): [m3.m-3] New soil moisture in each soil voxel
        - out_fluxes (List[float]): [mm] Amount of water really leaving each voxel
    """
    thetas_new = []
    fluxes_actual = []
    for theta, out_flux, vox, theta_min in zip(thetas, out_fluxes, soil, thetas_min):
        if theta < theta_min:
            fluxes_actual.append(0.)
            thetas_new.append(theta)
        else:
            theta_new = theta - out_flux / vox.volume
            if theta_new < theta_min:
                fluxes_actual.append((theta - theta_min) * vox.volume)
                thetas_new.append(theta_min)
            else:
                fluxes_actual.append(out_flux)
                thetas_new.append(theta_new)

    return thetas_new, fluxes_actual


def water_balance(soil, thetas, evaporation, supply, plant_uptake):
    """Compute the balance of water in each voxel.

    Args:
        soil (list[SoilVoxel]): Column of Soil. First element being the top most voxel
        thetas (List[float]): [m3.m-3] soil moisture in each voxel of soil
        evaporation (List[float]): [m3] Potential evaporation amount from each voxel
        supply (List[float]): [m3] Potential water input (irrigation and rain) in each voxel
        plant_uptake (List[float]): [m3] Potential plant uptake in each voxel

    Returns:
        (tuple):
         - thetas (List[float]): [m3.m-3] New soil moisture in each soil voxel
         - drain (List[float]): [mm] Amount of water leaving each voxel by gravity
         - evap (List[float]): [mm] Actual evaporation
         - supply (List[float]): [mm] Actual water entering each voxel
         - uptake (List[float]): [mm] Actual water taken up by plants in each voxel
    """
    thetas, drain = compute_drain(soil, thetas)
    thetas, supply_actual = compute_water_input(soil, thetas, supply, [vox.theta_sat for vox in soil])
    thetas, uptake_actual = compute_water_output(soil, thetas, plant_uptake, [vox.theta_pwp for vox in soil])
    thetas, evaporation_actual = compute_water_output(soil, thetas, evaporation, [vox.theta_res for vox in soil])

    return thetas, drain, evaporation_actual, supply_actual, uptake_actual
