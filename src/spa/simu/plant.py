from math import nan

from agrosim.process import Process, one_hour

from ..formalism.plant import light_stress, wp_stress


class ProcessTranspi(Process):
    def update(self, t, model, params):
        atm = model.atm

        if model.plant.growth_factor < 1e-3:
            model.plant.potential = nan
            model.plant.transpi_pot = 0.
        else:
            k_soil = params.soil.k_sat * model.soil.krs[params.soil.root_voxel] * model.plant.growth_factor
            k_root = params.plant.k_root_max * model.plant.growth_factor
            k_dark = params.plant.k_dark_max * model.plant.growth_factor
            k_sun = params.plant.k_sun_max * model.plant.growth_factor

            psi_soil = model.soil.potentials[params.soil.root_voxel]
            if psi_soil <= params.plant.pot_min:  # fully close stomata
                model.plant.potential = params.plant.pot_min
                model.plant.transpi_pot = 0.
            elif psi_soil <= atm.potential:  # atmosphere saturated with water
                model.plant.potential = psi_soil
                model.plant.transpi_pot = 0.
            else:
                # K between leaf and atm
                k_stom_rg = k_dark + (k_sun - k_dark) * light_stress(atm.rg, params.plant.light_sat)
                # K between soil and leaf
                k_plant = k_root  # (k_root * k_soil) / (k_root + k_soil)

                k_stom = k_stom_rg
                psi_xylem = (atm.potential * k_stom + psi_soil * k_plant) / (k_stom + k_plant)
                # for i in range(10):
                #     k_stom = k_stom_rg * wp_stress(psi_xylem, -0.2, 5.)
                #     psi_xylem = (atm.potential * k_stom + psi_soil * k_plant) / (k_stom + k_plant)

                if psi_xylem < params.plant.pot_min:  # close stomata
                    k_stom = k_plant * (psi_soil - params.plant.pot_min) / (params.plant.pot_min - atm.potential)
                    psi_xylem = params.plant.pot_min

                model.plant.potential = psi_xylem
                model.plant.transpi_pot = k_stom * (model.plant.potential - atm.potential)

        return t + one_hour


class ProcessGrowth(Process):
    def update(self, t, model, params):
        doy = t.timetuple().tm_yday

        if doy < params.plant.seeding:
            gf = 0.
        elif doy < params.plant.maturity:
            mat_frac = (doy - params.plant.seeding) / (params.plant.maturity - params.plant.seeding)
            gf = mat_frac ** 3
        elif doy < params.plant.senescence:
            gf = 1.
        else:
            gf = 0.

        model.plant.growth_factor = gf
        return t + one_hour
