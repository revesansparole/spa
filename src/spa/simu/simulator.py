from agrosim.scheduler import Scheduler

from . import atm
from . import itk
from . import plant
from . import soil
from . import stitching


class Simulator(Scheduler):

    def __init__(self, model, params):
        """Constructor

        Args:
            model (Model): current state of the model at t_ini (will be modified by run method)
            params (Params): Params object constructed from default params merged with inputs
        """
        super().__init__(params.weather[0].date, model, params)

        # Update state variable for the coming period
        self.add_process(atm.ProcessWeather(params.weather))
        self.add_process(atm.ProcessWaterPotential())
        self.add_process(soil.ProcessUpdateWaterContent())
        self.add_process(soil.ProcessWaterPotential())
        self.add_process(soil.ProcessHydraulicConductivity())
        self.add_process(plant.ProcessGrowth())

        # User interactions related processes
        self.add_process(itk.ItkProcess(params.planif, params.weather))

        # Water fluxes
        self.add_process(soil.ProcessEvaporation())
        self.add_process(plant.ProcessTranspi())
        self.add_process(stitching.TranspiUptakePotProcess())
        self.add_process(soil.ProcessWaterBalance())
        self.add_process(stitching.UptakeTranspiProcess())

    def run(self, time_end=None):
        if time_end is None:
            time_end = self.params().weather[-1].date

        return super().run(time_end)
