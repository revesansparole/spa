"""
Set of functions related to the modelling of processes in soil water.
"""
from agrosim.process import Process, one_hour
from spa.formalism.soil import vg_psi, vg_kr
from weather.fao import eto_hourly

from ..formalism.soil import water_balance


class ProcessEvaporation(Process):
    """Compute amount of evaporation from the soil.

    Liberaly adapted from reference :)

    References: https://agupubs.onlinelibrary.wiley.com/doi/full/10.1029/2018WR024050
    """

    def update(self, t, model, params):
        atm = model.atm
        eto = eto_hourly(t,
                         params.field.latitude,
                         params.field.longitude,
                         atm.temp,
                         atm.rg * 3600e-6,  # [W.m-2] to [MJ.m-2.h-1]
                         atm.rh,
                         atm.ws)

        evap_pot = 0.2 * eto
        model.soil.evap_pot = evap_pot * 1e-3 * params.field.area  # [mm.h-1] to [m3.h-1]

        return t + one_hour


class ProcessWaterBalance(Process):
    """Compute the balance of water in the soil.
    """

    def update(self, t, model, params):
        # evaporation
        evap_pot = [0] * len(params.soil.voxels)
        evap_pot[0] = model.soil.evap_pot

        # water supply (irrigation + rain)
        supply_pot = list(model.soil.irrig)
        supply_pot[0] += model.atm.rain * params.field.area * 1e-3  # [mm] to [m3] for the whole field

        # uptake by plant
        uptake_pot = [0] * len(params.soil.voxels)
        uptake_pot[params.soil.root_voxel] = model.soil.uptake_pot

        # perform water cycle
        thetas_new, drain, evap, supply, uptake = water_balance(params.soil.voxels,
                                                                model.soil.thetas,
                                                                evap_pot,
                                                                supply_pot,
                                                                uptake_pot)

        model.soil.wfs = [(th_new - th) * vox.volume
                          for th_new, th, vox in zip(thetas_new, model.soil.thetas, params.soil.voxels)]  # [m3]

        # all drain fluxes between voxels of soil correspond to water transfers
        # inside the soil compartment and not part of the overall balance
        model.soil.drainage = drain[-1]  # [m3]

        # evaporation occurs only at the uppermost soil layer
        model.soil.evap = evap[0]  # [m3]

        # actual runoff correspond to all supply that didn't make it into the soil
        model.soil.runoff = sum(flux_pot - flux for flux_pot, flux in zip(supply_pot, supply))  # [m3]

        # Actual uptake allowed for plants
        model.soil.uptake = sum(uptake)  # [m3]

        return t + one_hour


class ProcessUpdateWaterContent(Process):
    """Update water content in each voxel according to previously computed fluxes.
    """

    def update(self, t, model, params):
        for i, wc in enumerate(model.soil.thetas):
            model.soil.thetas[i] = wc + model.soil.wfs[i] / params.soil.voxels[i].volume

        return t + one_hour


class ProcessWaterPotential(Process):
    """Estimate matric water potential in each voxel.
    """

    def update(self, t, model, params):
        model.soil.potentials = [vg_psi(th, vox.theta_res, vox.theta_sat, 1 / vox.alpha, 1 / vox.n)
                                 for vox, th in zip(params.soil.voxels, model.soil.thetas)]

        return t + one_hour


class ProcessHydraulicConductivity(Process):
    """Estimate hydraulic conductivity in each voxel.
    """

    def update(self, t, model, params):
        model.soil.krs = [vg_kr(th, vox.theta_res, vox.theta_sat, vox.n)
                          for vox, th in zip(params.soil.voxels, model.soil.thetas)]

        return t + one_hour
