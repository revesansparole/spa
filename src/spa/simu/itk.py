"""
Processes related to grower practices
"""
from agrosim.process import Process, one_hour

MONDAY = 1


class ItkProcess(Process):
    def __init__(self, planif, weather):
        super().__init__()
        self.planif = planif
        self.irrigs = []
        self.weather = list(weather)

    def update(self, t, model, params):
        if t.isoweekday() == MONDAY and t.hour == 0:
            forecast = []
            for day_ind in range(7):
                day_weather = self.weather[day_ind * 24:(day_ind + 1) * 24]
                if day_weather:
                    forecast.append(dict(
                        date=day_weather[0].date,
                        rain=sum(e.rain for e in day_weather),
                        rg=sum(e.rg for e in day_weather),
                        rh=sum(e.rh for e in day_weather) / 24,
                        tmax=max(e.temp for e in day_weather),
                        tmin=min(e.temp for e in day_weather),
                        ws=sum(e.ws for e in day_weather) / 24,
                    ))

            planification = self.planif(t, forecast)
            assert len(planification) == 7

            self.irrigs = []
            threshold = 2e-3 * params.field.area
            for amount in planification:
                sub = []
                while amount > threshold:
                    sub.append(threshold)
                    amount -= threshold

                sub.append(amount)
                sub += [0.] * (24 - len(sub))
                assert len(sub) == 24  # TODO rm

                self.irrigs.extend(sub)

        self.weather.pop(0)

        model.soil.irrig = [0.] * len(params.soil.voxels)
        if self.irrigs:
            model.soil.irrig[0] = self.irrigs.pop(0)

        return t + one_hour
