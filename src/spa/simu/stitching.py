"""
Process to link variables between soil plant and atm
"""
from agrosim.process import Process, one_hour


class TranspiUptakePotProcess(Process):
    def update(self, t, model, params):
        model.soil.uptake_pot = model.plant.transpi_pot * params.field.density * params.field.area

        return t + one_hour


class UptakeTranspiProcess(Process):
    def update(self, t, model, params):
        model.plant.transpi = model.soil.uptake / params.field.area / params.field.density

        return t + one_hour
