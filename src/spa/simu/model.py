from agrosim.model import ModelBase
from spa.params import Params, StateParams


class AtmModel(ModelBase):
    """Model representation of atmosphere"""

    def __init__(self):
        super().__init__()

        self.rain = 0.
        """[mm] Total amount of rainfall reaching the ground between now and now + dt.
        """

        self.rg = 0.
        """[W.m-2] Average light intensity between now and now + dt.
        """

        self.rh = 0.
        """[%] Average relative humidity (between 0 and 100) between now and now + dt.
        """

        self.temp = 0.
        """[°C] Average temperature between now and now + dt.
        """

        self.ws = 0.
        """[m.s-1] Average wind speed for the period between now and now + dt.
        """

        self.potential = 0.
        """[MPa] Average water potential for the period between now and now + dt.
        """


class PlantModel(ModelBase):
    """Model representation of crop"""

    def __init__(self):
        super().__init__()

        self.growth_factor = 0.
        """[-] growth factor 0 at seeding and 1 at maturity
        """

        self.potential = 0.
        """[MPa] xylem water potential
        """

        self.transpi = 0.
        """[m3.plant-1] Total amount of transpiration for the period between now
        and now + dt for a single plant.
        """

        self.transpi_pot = 0.
        """[m3.plant-1] Potential amount of transpiration for the period between
        now and now + dt for a single plant.
        """


class SoilModel(ModelBase):
    def __init__(self):
        super().__init__()

        self.drainage = 0.
        """[m3] Water lost from the lowermost soil layer by drainage for the period
        between now and now + dt.
        """

        self.evap = 0.
        """[m3] Water lost from the soil by evaporation for the period between now
        and now + dt.
        """

        self.evap_pot = 0.
        """[m3] Amount of water that evaporates from the top most layer of soil
        for the period between now and now + dt.
        """

        self.irrig = []
        """[m3] Amount of irrigation/rain reaching each soil voxel for the period
        between now and now + dt.
        """

        self.runoff = 0.
        """[m3] Water lost by runoff on the surface of the soil for the period
        between now and now + dt..
        """

        self.uptake = 0.
        """[m3] Water uptake by roots for the period between now and now + dt.
        """

        self.uptake_pot = 0.
        """[m3] Water that roots would be happy to uptake for the period between
        now and now + dt.
        """

        self.potentials = []
        """[MPa] Matric water potential per voxel
        """

        self.krs = []
        """[-] Relative hydraulic conductivity per voxel
        """

        self.thetas = []
        """[m3.m-3] Soil water content per voxel of soil supposed constant over
        the period between now and now + dt..
        """

        self.wfs = []
        """[m3.day-1] Water flux entering each voxel (negative if water leaves
        the voxel) for the period between now and now + dt.
        """


class Model(ModelBase):
    def __init__(self, params):
        """Initialize new model

        Args:
            params (Params): simulation parameters to initialize model
        """
        super().__init__()

        self.atm = AtmModel()
        self.plant = PlantModel()
        self.soil = SoilModel()

        self.restore_state(params.state, params)

    def save_state(self):
        """Store current state to restore it later

        Returns:
            (StateParams)
        """
        return StateParams(
            # water
            thetas=list(self.soil.thetas),
            wfs=list(self.soil.wfs),
        )

    def restore_state(self, state, params):
        """Restore state of model

        Args:
            state (StateParams): Previous saved state
            params (Params): simulation params

        Returns:
            (None): modify model in place
        """
        # water
        self.soil.thetas = list(state.thetas)
        self.soil.wfs = list(state.wfs)
