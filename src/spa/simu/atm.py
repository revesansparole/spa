"""
Processes related to atmospheric part
"""
from agrosim.process import Process, one_hour
from ..formalism.weather import water_potential


class ProcessWeather(Process):
    """Extract current weather params into model
    """

    def __init__(self, weather):
        super().__init__()
        self.weather = list(weather)

    def update(self, t, model, params):
        entry = self.weather.pop(0)
        atm = model.atm
        atm.rain = entry.rain
        atm.rg = entry.rg / 3600 * 1e6  # [MJ.m-2] to [W.m-2]
        atm.rh = entry.rh
        atm.temp = entry.temp
        atm.ws = entry.ws

        if len(self.weather) > 0:
            return self.weather[0].date


class ProcessWaterPotential(Process):
    """Compute water potential of atmosphere
    """

    def update(self, t, model, params):
        atm = model.atm

        atm.potential = water_potential(atm.temp, atm.rh)

        return t + one_hour
