from agrosim.errors import SimError
from agrosim.json_schema import schema
from agrosim.jsonschema_extended import validator_reader
from soil.pedotransfer import theta_fc, theta_sat, theta_wp
from soil.texture import texture as tex_fractions
from spa.formalism.soil import eval_alpha
from spa.params import Params


def verify_input(params):
    """Check inputs are in correct range.

    Raises: SimError
      When a value is not in predefined range

    Args:
        params (Params):

    Returns:
        None
    """
    if params.field.crop not in ('courgette',):
        raise SimError(code='crop', msg=f"crop name '{params.field.crop}' not recognized")

    if not (-55 <= params.field.latitude <= 55):
        raise SimError(code='latitude', msg="the latitude is not between -55 and 55 degrees included")


def instantiate_params(inputs, debug=False, extra_params=None):
    """Instantiate Params for specific crop

    Args:
        inputs (dict): inputs values
        debug (bool): whether to test params
        extra_params (dict): Extra inputs

    Returns:
        (Params)
    """
    params = Params()
    if debug:
        sch = schema(params, require='inputs')
        validator_reader(sch).validate(inputs)

    # override default params values with custom ones
    params.update_from_dict(inputs)
    if extra_params:
        params.update_from_dict(extra_params)

    return params


def soil_from_texture(params, texture, om_fraction=0.02):
    """Modify soil section to reflect texture

    Args:
        params (Params): spa.params.Params
        texture (str): name of texture
        om_fraction (float): [-] fraction of organic matter

    Returns:
        (None): modify params in place
    """
    clay_fraction, sand_fraction = tex_fractions(texture)

    # modify only root layer
    vox_params = params.soil.voxels[params.soil.root_voxel]

    vox_params.theta_sat = theta_sat(clay_fraction, sand_fraction, om_fraction)
    vox_params.theta_fc = theta_fc(clay_fraction, sand_fraction, om_fraction)
    vox_params.theta_pwp = theta_wp(clay_fraction, sand_fraction, om_fraction)
    vox_params.theta_res = vox_params.theta_pwp * 0.99

    # vg params
    vox_params.alpha = eval_alpha(params.plant.psi_pwp,  # [MPa]
                                  vox_params.theta_pwp,
                                  vox_params.theta_res,
                                  vox_params.theta_sat,
                                  1 / vox_params.n)
