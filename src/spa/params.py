"""
Set of parameters for SPA simulator
"""
from dataclasses import dataclass, field as dcf
from typing import List

from agrosim.params import ParamsBase, SimuParams
from weather.io import WeatherEntry


@dataclass
class FieldParams(ParamsBase):
    """Parameters related to the simulated field
    """

    area: float = None
    """[m2] Surface of field
    """

    density: float = None
    """[plant.m-2] Density of plantation
    """

    crop: str = None
    """[-] Name of crop to simulate
    """

    latitude: float = None
    """[°] Latitude of field (positive North)
    """

    longitude: float = None
    """[°] Longitude of field (positive East)
    """

    variety: str = None
    """[-] Variety of crop planted
    """


@dataclass
class PlantParams(ParamsBase):
    seeding: int = 105  # year-04-15
    """Day of year for seeding
    """

    plantation: int = 135  # year-05-15
    """Day of year for trans-planting seedling
    """

    maturity: int = 166  # year-06-15
    """Day of year when plants reach maturity
    """

    senescence: int = 273  # year-09-30
    """Day of year when plants die
    """

    pot_min: float = -1.5
    """[MPa] minimum xylem potential acceptable for this species
    """

    psi_pwp: float = -1.5
    """[MPa] permanent wilting point
    """

    light_sat: float = 400
    """[W.m-2] light intensity above which photosynthesis saturates
    """

    k_root_max: float = 7e-3
    """[m3.h-1.MPa-1.plant-1] overall root conductance
    """

    k_dark_max: float = 1e-7
    """[m3.h-1.MPa-1.plant-1] overall canopy conductance in the dark
    """

    k_sun_max: float = 2.6e-6
    """[m3.h-1.MPa-1.plant-1] overall canopy conductance during day time
    """


@dataclass
class SoilVoxelParams(ParamsBase):
    """Parameters for a single voxel of soil
    """
    thickness: float = None
    """[mm] thickness of soil layer.
    """

    volume: float = None
    """[m3] volume of voxel
    """

    theta_fc: float = None
    """[m3.m-3] soil moisture at field capacity.
    """

    theta_pwp: float = None
    """[m3.m-3] soil moisture at plant wilting point.
    """

    theta_res: float = None
    """[m3.m-3] residual soil moisture.
    """

    theta_sat: float = None
    """[m3.m-3] soil moisture at saturation.
    """

    alpha: float = None
    """[MPa-1] scaling parameter for VG formalism
    """

    n: float = None
    """[-] shape parameter for VG formalism
    """


@dataclass
class SoilParams(ParamsBase):
    """Parameters for soil represented as a set of voxels
    """
    voxels: List[SoilVoxelParams] = dcf(default_factory=list)
    """List of different soil voxels
    """

    root_voxel: int = 1
    """[-] id of voxel containing roots
    """

    k_sat: float = 7e-3 / 7e-3
    """[m3.h-1.MPa-1.plant-1] Overall soil hydraulic conductivity
    at saturation.
    """


@dataclass
class WeatherEntry(WeatherEntry, ParamsBase):
    pass


@dataclass
class StateParams(ParamsBase):
    """Parameters related to current state of model before simulation"""
    #############################
    #
    # water related
    thetas: List[float] = dcf(default_factory=list)
    """store :attr:`spa.simu.model.SoilModel.thetas`.
    """

    wfs: List[float] = dcf(default_factory=list)
    """store :attr:`spa.simu.model.SoilModel.wfs`.
    """


@dataclass
class Params(SimuParams):
    """Set of parameters used by simulator
    """
    field: FieldParams = dcf(default_factory=FieldParams)

    plant: PlantParams = dcf(default_factory=PlantParams)

    soil: SoilParams = dcf(default_factory=SoilParams)

    weather: List[WeatherEntry] = dcf(default_factory=list)

    state: StateParams = dcf(default_factory=StateParams)

    planif: str = "Callable[[datetime, List], List]"
