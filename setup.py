# {# pkglts, pysetup.kwds
# format setup arguments

from pathlib import Path

from setuptools import setup, find_packages


short_descr = "Soil Plant Atmosphere model"
readme = open('README.rst').read()
history = open('HISTORY.rst').read()

# find packages
pkgs = find_packages('src')

src_dir = Path("src/spa")

data_files = []
for pth in src_dir.glob("**/*.*"):
    if pth.suffix in ['.json', '.ini', '.csv']:
        data_files.append(str(pth.relative_to(src_dir)))

pkg_data = {'spa': data_files}

setup_kwds = dict(
    name='spa',
    version="2.1.0",
    description=short_descr,
    long_description=readme + '\n\n' + history,
    author="itk",
    author_email="agro@itk.fr",
    url='https://gitlab.itkweb.fr/jchopard/spa',
    license='private',
    zip_safe=False,

    packages=pkgs,
    
    package_dir={'': 'src'},
    
    
    package_data=pkg_data,
    setup_requires=[
        "pytest-runner",
        ],
    install_requires=[
        "agrosim>=5.0, <5.1",
        "numpy",
        "pandas",
        "soil>=2.7, <2.8",
        "weather>=2.0, <2.1",
        ],
    tests_require=[
        "coverage",
        "pytest",
        "pytest-cov",
        "pytest-mock",
        ],
    entry_points={},
    keywords='itk',
    
    classifiers=[
        "Development Status :: 2 - Pre-Alpha",
        "Intended Audience :: Developers",
        "License :: OSI Approved :: BSD License",
        "Natural Language :: English",
        "Programming Language :: Python",
        "Programming Language :: Python :: 3",
        "Programming Language :: Python :: 3 :: Only",
        "Programming Language :: Python :: 3.8",
    ],
    )
# #}
# change setup_kwds below before the next pkglts tag

# do not change things below
# {# pkglts, pysetup.call
setup(**setup_kwds)
# #}
