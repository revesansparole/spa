=======
Credits
=======

Development Lead
----------------

.. {# pkglts, doc.authors

* itk, <agro@itk.fr>

.. #}

Contributors
------------

.. {# pkglts, doc.contributors

* jchopard <jerome.chopard@itk.fr>
* Jerome Chopard <jerome.chopard@itk.fr>

.. #}
