# {# pkglts, test.pytest_import
import os

import pytest
# #}


# {# pkglts, test.pytest_cmdline_preparse
def pytest_cmdline_preparse(args):
    if 'PYCHARM_HOSTED' not in os.environ:
        args.append("--cov=spa")
# #}


# {# pkglts, test.pytest_addoption
def pytest_addoption(parser):
    parser.addoption("--runslow", action="store_true",
                     default=False, help="run slow tests")
# #}
# {# pkglts, test.pytest_addoption_external, after test.pytest_addoption
    parser.addoption("--external", action="store_true",
                     default=False, help="run tests that call an external API")
# #}


# {# pkglts, test.pytest_configure
def pytest_configure(config):
    config.addinivalue_line(
        "markers", "slow: marks tests as slow (deselect with '--runslow')"
    )
# #}
# {# pkglts, test.pytest_configure_external, after test.pytest_configure
    config.addinivalue_line(
        "markers", "external: marks tests as calling an external API (deselect with '--external')"
    )
# #}


# {# pkglts, test.pytest_collection
def pytest_collection_modifyitems(config, items):
    if not config.getoption("--runslow"):  # skip slow tests
        skip_slow = pytest.mark.skip(reason="need --runslow option to run")
        for item in items:
            if "slow" in item.keywords:
                item.add_marker(skip_slow)
# #}
# {# pkglts, test.pytest_collection_external, after test.pytest_collection
    if not config.getoption("--external"):  # skip slow tests
        skip_external = pytest.mark.skip(reason="need --external option to run")
        for item in items:
            if "external" in item.keywords:
                item.add_marker(skip_external)
# #}



