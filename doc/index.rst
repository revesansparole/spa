Welcome to spa's documentation!
====================================================

.. toctree::
   :caption: Contents
   :maxdepth: 2

   readme
   installation
   usage
   management

.. toctree::
   :caption: User's documentation
   :maxdepth: 2

   user/index
   _gallery/index

.. toctree::
   :caption: Developper's documentation
   :maxdepth: 4

   Sources <_dvlpt/modules>

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`

Back to Agrodoc home page.

.. toctree::
    :maxdepth: 1

    AgroDoc <http://agro.pages.itkweb.fr/doc/index.html>
