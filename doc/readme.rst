Overview
========

.. {# pkglts, itkpkg

.. image:: http://jchopard.pages.itkweb.fr/spa/_images/badge_doc.svg
    :alt: Documentation status
    :target: http://jchopard.pages.itkweb.fr/spa/

.. image:: http://jchopard.pages.itkweb.fr/spa/_images/badge_pkging_pip.svg
    :alt: PyPI version
    :target: http://nexus3.itkweb.fr/#browse/browse:pysim:spa%2F2.1.0

.. image:: http://jchopard.pages.itkweb.fr/spa/_images/badge_pkging_conda.svg
    :alt: Conda version
    :target: https://anaconda.org/itk/spa


develop: |dvlp_build|_ |dvlp_coverage|_

.. |dvlp_build| image:: https://gitlab.itkweb.fr/jchopard/spa/badges/develop/pipeline.svg
.. _dvlp_build: https://gitlab.itkweb.fr/jchopard/spa/commits/develop

.. |dvlp_coverage| image:: https://gitlab.itkweb.fr/jchopard/spa/badges/develop/coverage.svg
.. _dvlp_coverage: https://gitlab.itkweb.fr/jchopard/spa/commits/develop


master: |master_build|_ |master_coverage|_

.. |master_build| image:: https://gitlab.itkweb.fr/jchopard/spa/badges/master/pipeline.svg
.. _master_build: https://gitlab.itkweb.fr/jchopard/spa/commits/master

.. |master_coverage| image:: https://gitlab.itkweb.fr/jchopard/spa/badges/master/coverage.svg
.. _master_coverage: https://gitlab.itkweb.fr/jchopard/spa/commits/master

.. #}

Soil Plant Atmosphere model
