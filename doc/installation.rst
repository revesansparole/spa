============
Installation
============

To install the package follow the instructions on `How to use a python package <http://agro.pages.itkweb.fr/doc/use/python_package.html>`_.
Especially if you want to rely on any other method than conda or if you intend to
install the package from sources for further development.
Below is just a reminder of the commands with the name of this package for
convenience. If you encounter any trouble in the installation process, have a
look at `conda troubleshooting <http://agro.pages.itkweb.fr/doc/use/conda.html#troubleshooting>`_.

If you only want to use the package::

    $ activate myenv
    (myenv) $ conda install spa
